import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TemplateMainComponent } from './components/template-main/template-main.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { GeneralComponent } from './components/general/general.component';
import { ModeComponent } from './components/mode/mode.component';
import { AttributeComponent } from './components/attribute/attribute.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    TemplateMainComponent,
    SidebarComponent,
    GeneralComponent,
    ModeComponent,
    AttributeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [GeneralComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
