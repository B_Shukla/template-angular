import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { SharedDataService } from "../../services/shared-data.service";
@Component({
  selector: 'app-mode',
  templateUrl: './mode.component.html',
  styleUrls: ['./mode.component.scss']
})
export class ModeComponent implements OnInit {
  modeList = ['a'];
  modeForm: FormGroup;
  currentTemplateData: any;
  constructor(private fb: FormBuilder,
    private sharedDataService: SharedDataService) { }

  ngOnInit(): void {
    this.modeForm = this.fb.group(
      {
        name: '',
        formula: '',
        arr: this.fb.array([this.createArray()])
      }
    );
    this.currentTemplateData = this.sharedDataService.currentTemplateData;
    if (this.currentTemplateData && this.currentTemplateData.mode != null) {
      this.initializeFormFields(this.currentTemplateData);
    }
  }

  initializeFormFields(data: any) {
    this.modeForm.controls['name'].setValue(this.currentTemplateData.mode.name);
    this.modeForm.controls['formula'].setValue(this.currentTemplateData.mode.formula);
    (this.currentTemplateData.mode.arr).forEach(element => {
      this.modeForm.controls['variable'].setValue(element.variable);
      this.modeForm.controls['channel'].setValue(element.channel);
    });
  }

  createArray(): FormGroup {
    return this.fb.group({
      variable: '',
      channel: '',
    })
  }

  addRow() {
    var array = this.modeForm.get('arr') as FormArray;
    array.push(this.createArray());
  }

  addItem() {
    this.modeList.push('a');
  }

  deleteRow() {
    var array = this.modeForm.get('arr') as FormArray;
    array.removeAt(array.length - 1);
  }

  save() {
    if (this.sharedDataService.currentTemplateData) {
      this.sharedDataService.currentTemplateData.mode = this.modeForm.value;
    }
    else {
      this.sharedDataService.currentTemplateData = { general: null, attribute: null, mode: this.modeForm.value };
    }
    this.sharedDataService.successAlert();
  }
}
