import { Component, OnInit } from '@angular/core';
import { SharedDataService } from "../../services/shared-data.service";
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  templateList:any;
  constructor(private sharedDataService: SharedDataService) { }

  ngOnInit(): void {
    this.sharedDataService.templateData$.subscribe((res: any) => {
      // console.log(res);
      this.templateList = res;
    })
  }

}
