import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SharedDataService } from "../../services/shared-data.service";

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.scss']
})
export class GeneralComponent implements OnInit {
  generalForm: FormGroup;
  initialFormValue: any;
  currentTemplateData: any;
  constructor(private fb: FormBuilder,
    private sharedDataService: SharedDataService) { }

  ngOnInit(): void {
    this.generalForm = this.fb.group(
      {
        name: ['', Validators.required],
        description: '',
        data1: '10',
        data2: 'Minutes'
      }
    )
    this.initialFormValue = this.generalForm.value;
    this.currentTemplateData = this.sharedDataService.currentTemplateData;
    if(this.currentTemplateData  && this.currentTemplateData.general != null){
      this.initializeFormFields(this.currentTemplateData);
    }
  }

  initializeFormFields(data: any) {
    this.generalForm.controls['name'].setValue(this.currentTemplateData.general.name);
    this.generalForm.controls['description'].setValue(this.currentTemplateData.general.description);
    this.generalForm.controls['data1'].setValue(this.currentTemplateData.general.data1);
    this.generalForm.controls['data2'].setValue(this.currentTemplateData.general.data2);
  }

  save() {
    if(this.sharedDataService.currentTemplateData){
      this.sharedDataService.currentTemplateData.general = this.generalForm.value;
    }
    else{
      this.sharedDataService.currentTemplateData = { general: this.generalForm.value, attribute: null, mode: null };
    }
    this.sharedDataService.successAlert(); 
    // this.sharedDataService.currentTemplateData$.next({ general: this.generalForm.value, attribute: null, mode: null });
  }

  submit() {
    if(this.sharedDataService.currentTemplateData){
      this.sharedDataService.templateData.push(this.sharedDataService.currentTemplateData);
    }
    else{
      this.sharedDataService.templateData.push({ general: this.generalForm.value, attribute: null, mode: null });
    }
    // this.sharedDataService.templateData.push({ general: this.generalForm.value, attribute: null, mode: null });
    this.sharedDataService.templateData$.next(this.sharedDataService.templateData);
    this.sharedDataService.currentTemplateData = undefined;
    this.generalForm.reset(this.initialFormValue);
  
    this.sharedDataService.successAlert('submit successful!');
  }

  cancel() {
    this.generalForm.reset(this.initialFormValue)
  }
}
