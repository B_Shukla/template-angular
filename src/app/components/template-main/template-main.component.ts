import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-template-main',
  templateUrl: './template-main.component.html',
  styleUrls: ['./template-main.component.scss']
})
export class TemplateMainComponent implements OnInit {
  formType = "general";
  constructor() { }

  ngOnInit(): void {
  }

  changeForm(type: string) {
    this.formType = type;
  }
}
