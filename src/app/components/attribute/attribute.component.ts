import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SharedDataService } from 'src/app/services/shared-data.service';

@Component({
  selector: 'app-attribute',
  templateUrl: './attribute.component.html',
  styleUrls: ['./attribute.component.scss']
})
export class AttributeComponent implements OnInit {
  attributeForm: FormGroup;
  currentTemplateData:any;

  constructor(private fb: FormBuilder,
    private sharedDataService: SharedDataService) { }

  ngOnInit(): void {
    this.attributeForm = this.fb.group(
      {
        tagArr: this.fb.array([this.createArray()])
      }
    );
    this.currentTemplateData = this.sharedDataService.currentTemplateData;
    if(this.currentTemplateData && this.currentTemplateData.attribute != null){
      this.initializeFormFields(this.currentTemplateData);
    }
  }

  createArray(): FormGroup {
    return this.fb.group({
      tagname: '',
      aliasname: '',
      tagtype: '',
      high: '',
      low: '',
    })
  }

  initializeFormFields(data:any){
    (this.currentTemplateData.attribute.tagArr).forEach(element => {
      this.attributeForm.controls['tagname'].setValue(element.tagname);
      this.attributeForm.controls['aliasname'].setValue(element.aliasname);
      this.attributeForm.controls['tagtype'].setValue(element.tagtype);
      this.attributeForm.controls['high'].setValue(element.high);
      this.attributeForm.controls['low'].setValue(element.low);
    });
  }

  addRow() {
    var array = this.attributeForm.get('tagArr') as FormArray;
    array.push(this.createArray());
  }

  deleteRow(index: any) {
    var array = this.attributeForm.get('tagArr') as FormArray;
    array.removeAt(index);
  }

  save(){
    if(this.sharedDataService.currentTemplateData){
      this.sharedDataService.currentTemplateData.attribute = this.attributeForm.value;
    }
    else{
      this.sharedDataService.currentTemplateData = { general: null, attribute: this.attributeForm.value, mode: null };
    }
    this.sharedDataService.successAlert();
  }
}
