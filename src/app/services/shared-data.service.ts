import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { BehaviorSubject } from 'rxjs';

import Swal from 'sweetalert2/dist/sweetalert2.js';  

@Injectable({
  providedIn: 'root'
})
export class SharedDataService {
  constructor() { }
  currentTemplateData:any;
  templateData = [{
    general: {
      name: "template 1",
      description: '',
      data1: "",
      data2: ''
    },
    attribute: {
      tagArr:[{
        tagname: '',
        aliasname: '',
        tagtype: '',
        high: '',
        low: ''
      }]
    },
    mode: {
      name: '',
      formula: '',
      arr: [{
        variable: '',
        channel: ''
      }]
    }
  }]
  templateData$ = new BehaviorSubject(this.templateData);
  currentTemplateData$ = new Subject();

  successAlert(msg:any = 'saved successful! Click submit for final submission'){
    Swal.fire({  
      position: 'center',  
      icon: 'success',  
      title: msg,  
      showConfirmButton: false,  
      timer: 1500  
    }) ; 
  }
}
